<!doctype html>
<?php include('header.php'); ?>
<html>

<head>
    <title>Registreerimise vorm</title>
    <meta charset="utf-8">
</head>

<body>

    <form method="post" action="register.php">
    <div class="container" >
        <input type="hidden" name="aeg" value="2016-02-09">

        <table border="1" style="width:100%;">
            <h3>Registreerimise vorm</h3>

            <tr>
                <th>
                    <label for="username-id">
                        Soovitud kasutajanimi:
                    </label>
                </th>
                <td>
                    <input type="text" name="kasutajanimi" id="username-id" placeholder="Kasutajanimi" style="width:90%;" >
                </td>
            </tr>

            <tr>
                <th>
                    <label for="password-id">
                        Parool:
                    </label>
                </th>
                <td>
                    <input type="password" name="parool" id="password-id" placeholder="Parool" style="width:90%;">
                </td>
            </tr>

            <tr>
                <th>
                    <label for="password2-id">
                        Korda parooli:
                    </label>
                </th>
                <td>
                    <input type="password" name="password2" id="password2-id" placeholder="Parool" style="width:90%;">
                </td>
            </tr>
			
			<tr>
                <th>
                    <label for="nimi-id">
                    Sisesta eesnimi:
                    </label>
                </th>
                <td>
                    <input type="text" name="eesnimi" id="nimi-id" placeholder="Eesnimi" style="width:90%;">
                </td>
            </tr>
			
			<tr>
                <th>
                    <label for="perenimi-id">
                        Sisesta perekonnanimi:
                    </label>
                </th>
                <td>
                    <input type="text" name="perenimi" id="perenimi-id" placeholder="Perekonnanimi" style="width:90%;">
                </td>
            </tr>
			
			<tr>
                <th>
                    <label for="email-id">
                        Email:
                    </label>
                </th>
                <td>
                    <input type="email" name="email" id="email-id" placeholder="Email" style="width:90%;">
                </td>
            </tr>
			<tr>
                <th>
                    <label for="tel-id">
                        Tel:
                    </label>
                </th>
                <td>
                    <input type="tel" name="tel" id="tel-id" placeholder="Telefoni nr" style="width:90%;">
                </td>
            </tr>
			<tr>
                <th>
                    <label for="aadress-id">
                       aadress:
                    </label>
                </th>
                <td>
                    <input type="text" name="aadress" id="aadress-id" placeholder="aadress" style="width:90%;">
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <button type="submit">Registreeri</button>
                </td>
            </tr>

        </table>

    </form>
</div>
</body>

</html>