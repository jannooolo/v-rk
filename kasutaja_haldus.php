<?php
include('header.php');
include('nav.php');
?>
<div class="container">
	<?php
	if(logitudAdmin()){
		$kasutajad = kasutajad();
		$lines = 1;
		echo '<input type="checkbox" name="admin" id="admin" value="yes" checked onchange="showAdmin()">';
		echo '<input type="checkbox" name="aktiveeritud" id="aktiveeritud" value="yes" checked onchange="showActivated()">';
		echo '<form method="post" action="aktiveeri.php">';
		echo '<table style="width:100%"><th>Kasutajanimi</th><th>Eesnimi</th><th>Perenimi</th><th>E-mail</th><th>Telefon</th><th>Aadress</th><th>Roll</th><th>Aktiveeritud</th>';
			foreach($kasutajad as $kasutaja){
				echo '<tr ' . ($kasutaja['roll']=='admin' ? 'class="admin ' : 'class="' ) . ($kasutaja['aktiveeritud']==1 ? ' aktiveeritud"': '"') . '><td>' . $kasutaja['kasutajanimi'] . '</td><td>' . $kasutaja['eesnimi'] . '</td><td>' . $kasutaja['perenimi'] . '</td><td>' . $kasutaja['email'] . '</td><td>' . $kasutaja['tel'] . '</td><td>' . $kasutaja['aadress'] . '</td><td><input type="checkbox" value="yes" ' . ($kasutaja['roll']=='admin' ? 'checked' : '' ) . ' name="roll-' . $lines . '"></td><td><input type="checkbox" value="yes" ' . ($kasutaja['aktiveeritud']==1 ? 'checked': '' ) . ' name="aktiveeritud-' . $lines . '"></td><td><input type="hidden" name="id-' . $lines . '" value="' . $kasutaja['id'] . '"></td></tr>';
				$lines++;
			}
		echo '</table><input type="hidden" name="lines" value="' . $lines . '"><button type="submit">Salvesta</button></form>';
	}
	?>
</div>