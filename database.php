<?php
function yhendus () {
	$con = mysqli_connect("localhost", "test", "t3st3r123", "test");
	$con->set_charset("utf8");
	if ($con->connect_error) {
		die("Connection failed: " . $con->connect_error);
	} else {
    return $con;
  }
}

function registreeri ($kasutajanimi,$parool, $eesnimi, $perenimi, $email, $tel, $aadress) {
	$con = yhendus();
   
	$sql = "INSERT INTO oolo_users (kasutajanimi, parool,eesnimi,perenimi,email,tel,aadress,roll) VALUES (?, ?, ?, ?, ?, ?, ?, 'user');";
	$query = $con->prepare($sql);
	$query->bind_param('sssssss', $kasutajanimi,$parool, $eesnimi, $perenimi, $email, $tel, $aadress);
	$query->execute();

	$con->close();
}

function logisisse ($kasutajanimi, $parool) {
  startSession();
	$con = yhendus();
   
	$sql = "SELECT id, kasutajanimi, parool FROM oolo_users WHERE kasutajanimi = ? LIMIT 1;";
	$query = $con->prepare($sql);
	$query->bind_param('s', $kasutajanimi);
	$query->execute();
	 
	$result = $query->get_result();
	$row = $result->fetch_assoc();
  
  $con->close();
  
	if (isset($row) && $row["parool"] = $parool) {
		$_SESSION['id'] = preg_replace("/[^0-9]+/", "", $row['id']);
		$_SESSION['kasutajanimi'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["kasutajanimi"]);
		$_SESSION['parool'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["parool"]);
		return true;
	}
  
	return false;
}

function sisselogitud () {
  startSession();
  if (isset($_SESSION['id'], $_SESSION['kasutajanimi'], $_SESSION['parool'])) {
    $id = $_SESSION['id'];
    $kasutajanimi = $_SESSION['kasutajanimi'];
    $parool = $_SESSION['parool'];
    
		$con = yhendus();
		$sql = "SELECT parool FROM oolo_users WHERE id = ? LIMIT 1;";
		$query = $con->prepare($sql);
		$query->bind_param('i', $id);
		$query->execute();
		
		$result = $query->get_result();
		$row = $result->fetch_assoc();
		if (isset($row)) {
			if ($parool == $row['parool']){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function startSession () {
	$session_name = 'sessiaeg';
	$secure = false;
	$httponly = true;
	$cookieParams = session_get_cookie_params();
	session_set_cookie_params($cookieParams["lifetime"],
		$cookieParams["path"],
		$cookieParams["domain"],
		$secure,
		$httponly);
	session_name($session_name);
	session_start();
	session_regenerate_id(true);
}

function logivalja () {
		startSession();
		$_SESSION = array();
		$params = session_get_cookie_params();
		setcookie(session_name(),
			'', time() - 50000,
			$params["path"],
			$params["domain"],
			$params["secure"],
			$params["httponly"]);
		session_destroy();
	}

function tooted(){
	$con = yhendus();
	$sql = "SELECT * FROM oolo_tooted WHERE saadavus=true";
	$query = $con->prepare($sql);
	$query->execute();
	$result = $query->get_result();
	$tooted = array();
	while($row = $result->fetch_assoc()){
		$tooted[] = $row;
	}
	$con->close();
	return $tooted;
}
function saaToode($id){
	$con = yhendus();
	$sql = "SELECT * FROM oolo_tooted WHERE id=" . $id;
	$query = $con->prepare($sql);
	$query->execute();
	$result = $query->get_result();
	while($row = $result->fetch_assoc()){
		$toode = $row;
	}
	$con->close();
	return $toode;
}
function lisaToode($kategooria_id, $nimi, $kirjeldus, $hind, $kogus, $pilt){
	$con = yhendus();
	$sql = "INSERT INTO oolo_tooted(kategooria_id, nimi, kirjeldus, hind, kogus, pilt) VALUES(?,?,?,?,?,?)";
	$query = $con->prepare($sql);
	$query->bind_param('ssssss', $kategooria_id, $nimi, $kirjeldus, $hind, $kogus, $pilt);
	$result = $query->execute();
	if(!$result){
		$con->close();
		return false;
	}else{
		$con->close();
		return true;
	}
}
function muudaToode($id, $kategooria_id, $nimi, $kirjeldus, $hind, $kogus, $pilt){
	$con = yhendus();
	$sql = "UPDATE oolo_tooted SET kategooria_id = ?, nimi = ?, kirjeldus = ?, hind = ?, kogus = ?, pilt = ? WHERE id = " . $id;
	$query = $con->prepare($sql);
	$query->bind_param('issdis', $kategooria_id, $nimi, $kirjeldus, $hind, $kogus, $pilt);
	$result = $query->execute();
	if(!$result){
		$con->close();
		return false;
	}else{
		$con->close();
		return true;
	}
}
function lisaKategooria($nimi){
	$con = yhendus();
	$sql = "INSERT INTO oolo_kategooriad(nimi) VALUES('" . $nimi . "')";
	$query = $con->prepare($sql);
	$result = $query->execute();
	if(!$result){
		$con->close();
		return false;
	}else{
		$con->close();
		return true;
	}
}
function kategooriad(){
	$con = yhendus();
	$sql = "SELECT * FROM oolo_kategooriad";
	$query = $con->prepare($sql);
	$query->execute();
	$result = $query->get_result();
	$kategooriad = array();
	while($row = $result->fetch_assoc()){
		$kategooriad[] = $row;
	}
	$con->close();
	return $kategooriad;
}
function onAdmin($id){
	$con = yhendus();
	$sql = "SELECT roll FROM oolo_users WHERE id=" . $id . " AND roll='admin' LIMIT 1";
	$query = $con->prepare($sql);
	$query->execute();
	
	$result = $query->get_result();
	$row = $result->fetch_assoc();
	if(isset($row['roll']) && $row['roll'] == 'admin'){
		$con->close();
		return true;
	}else{
		$con->close();
		return false;
	}
}
function logitudAdmin(){
	if(sisselogitud() && onAdmin($_SESSION['id'])){
		return true;
	}else{
		return false;
	}
}
function kasutajad(){
	$con = yhendus();
	$sql = "SELECT * FROM oolo_users";
	$query = $con->prepare($sql);
	$query->execute();
	$result = $query->get_result();
	$kasutajad = array();
	while($row = $result->fetch_assoc()){
		$kasutajad[] = $row;
	}
	$con->close();
	return $kasutajad;
}
function aktiveeri_id($id){
	$con = yhendus();
	$sql = "UPDATE oolo_users SET aktiveeritud=1 WHERE id=". $id;
	$query = $con->prepare($sql);
	$result = $query->execute();
	if(!$result){
		$con->close();
		return false;
	}else{
		$con->close();
		return true;
	}
}
function muuda_roll($id, $roll){
	$con = yhendus();
	$sql = "UPDATE oolo_users SET roll='" . $roll . "' WHERE id=". $id . " AND aktiveeritud=1";
	$query = $con->prepare($sql);
	$result = $query->execute();
	if(!$result){
		$con->close();
		return false;
	}else{
		$con->close();
		return true;
	}
}
?>