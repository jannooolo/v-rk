  <?php include('header.php');
  include('nav.php'); ?>


  <script type="text/javascript"
      src="http://maps.google.com/maps/api/js?sensor=false"
  	src="http://www.skypeassets.com/i/scom/js/">
  </script>
  <script type="text/javascript">
    function initialize() {
      var position = new google.maps.LatLng(59.419454, 24.640806);
      var myOptions = {
        zoom: 14,
        center: position,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(
          document.getElementById("map_canvas"),
          myOptions);
   
      var marker = new google.maps.Marker({
          position: position,
          map: map,
          title:"This is the place."
      });  
   
      var contentString = ' <strong>Siin me asume!</strong> ';
      var infowindow = new google.maps.InfoWindow({
          content: contentString
      });
   
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
      });
   
    }
   
  </script>
  </head>
  <body onload="initialize()" background="taust.jpg" bgproperties="fixed">
  <div class="container bg-white">
    <div class="col-sm-6">
      <h3>Leia meid kaardilt</h3>
      <p>
      <div id="map_canvas" style="width:100%; height: 100%; min-height:500px"></div>
      </p>
    </div>
  <br />
  <div class="col-sm-6">
  <h3>Võta ühendust</h3>
    <p>Telefon <a href="tel:+37256279969">5627 9969</a></p>
    <p>Email - Janno.oolo@itcollege.ee</p>
    <p><img src="https://upload.wikimedia.org/wikipedia/commons/e/ec/Skype-icon-new.png" style="width:24px; height: auto;" alt="skype icon" /> <a href="skype:janno_oolo?call">Helista Skypes</a></p>
  </div>
</div>
</body>
</html>