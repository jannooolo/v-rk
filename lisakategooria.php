<?php
	require_once "database.php";
	if(logitudAdmin()){
		if(isset($_POST['nimi'])){
			if(!lisakategooria($_POST['nimi'])){
				$_SESSION['query_msg'] = 'Kategooria lisamine ebaõnnestus!';
			}else{
				$_SESSION['query_msg'] = 'Kategooria lisati andmebaasi!';
			}
		}
		header('Location: lisatoode.php');
	}
?>