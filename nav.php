<nav class="navbar navbar-default navbar-fixed-top">
        <div class="container" style="padding-bottom:0">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.php">Pealeht</a></li>
              <li><a href="pood.php">Pood</a></li>
			  <li><a href="lisatoode.php">Lisa tooteid</a></li>
			  <li><a href="kasutaja_haldus.php">kasutajad</a></li>
			  <li><a href="kontakt.php">kontakt</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="logout.php">Log out</a></li>
            </ul>
          </div>
        </div>
      </nav>