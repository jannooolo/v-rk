function showAdmin(){
	if($('.admin').is(':visible')){
		$('.admin').hide();
	}else{
		$('.admin').show();
	}
}
function showActivated(){
	$('.aktiveeritud').each(function(){
		if($(this).is(':visible') && !$(this).hasClass('.admin')){
			$(this).show();
		}else{
			$(this).hide();
		}
	});
}
