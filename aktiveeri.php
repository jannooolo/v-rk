<?php
require_once "database.php";
if(logitudAdmin()){
	$lines = $_POST['lines'];
	for($i = 1;$i < $lines; $i++){
		if(isset($_POST['roll-' . $i])){
			muuda_roll($_POST['id-' . $i], 'admin');
		}else{
			muuda_roll($_POST['id-' . $i], 'user');
		}
		if(isset($_POST['aktiveeritud-' . $i])){
			aktiveeri_id($_POST['id-' . $i]);
		}
	}
	header('Location: kasutaja_haldus.php');
}
?>