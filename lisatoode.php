<div class="container">
<?php
	include('header.php');
	include('nav.php');
	if(sisselogitud() && onAdmin($_SESSION['id'])): ?>
	<div class="col-md-3">
		<form method="post" action="lisatoodebaasi.php" enctype="multipart/form-data">

		<label for="nimi" style="display:block;margin-top:12px;">Nimi:</label>
		<input type="text" style="width:100%" id="nimi" name="nimi">
		<label for="kirjeldus" style="display:block;margin-top:12px;">Kirjeldus:</label>
		<input type="text"  style="width:100%"id="kirjeldus" name="kirjeldus"><br />
		<label for="kategooria" style="display:block;margin-top:12px;">Kategooria:</label>
		<select id="kategooria" style="width:100%" name="kategooria">
		<?php
			$kategooriad = kategooriad();
			foreach($kategooriad as $kategooria){
				echo '<option value="' . $kategooria['id'] . '">'. $kategooria['nimi'] . '</option>';
			}
		?>
		</select><br />
		<label for="hind" style="display:block;margin-top:12px;">Hind:</label>
		<input type="text" style="width:100%" id="hind" name="hind"><br />
		<label for="kogus" style="display:block;margin-top:12px;">Kogus:</label>
		<input type="text" style="width:100%" id="kogus" name="kogus"><br />
		<label for="pilt" style="display:block;margin-top:12px;">Pilt:</label>
		<input type="file" style="width:100%" id="pilt" name="pilt"><br />
		<input type="submit" class="btn" value="Lisa toode" name="submit">
		</form>
		<div id="query_msg"><?php echo (isset($_SESSION['query_msg']) ? $_SESSION['query_msg'] :'');unset($_SESSION['query_msg']); ?></div>
		<br />
		<br />
		<form method="post" action="lisakategooria.php">
			<button type="submit" class="btn" name="submit">Lisa kategooria</button>
		</form>
	</div>
		<?php $tooted = tooted(); $kategooriad = kategooriad(); ?>
		<div class="col-md-9">
			<table style="width:100%">
				<th>Pilt</th><th>Nimi</th><th>Kirjeldus</th><th>Kategooria</th><th>Hind</th><th>Kogus</th><th>Muuda</th>
				<?php foreach($tooted as $toode): ?>
				<tr>
					<td><img src="<?php echo $toode['pilt'] ?>" class="tootepilt"></td>
					<td><?php echo $toode['nimi'] ?></td>	
					<td><?php echo $toode['kirjeldus'] ?></td>	
					<td><?php 
						foreach($kategooriad as $kategooria){
							if($kategooria['id'] == $toode['kategooria_id']){
								echo $kategooria['nimi'];
								break;
							}
						} 
					?></td>	
					<td><?php echo $toode['hind'] ?></td>
					<td><?php echo $toode['kogus'] ?></td>
					<td><a href="muudatoode.php?id=<?php echo $toode['id'] ?>">Muuda</a></td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
			
	<?php else: ?>

	<div class="panel panel-danger">
	        <div class="panel-heading">
	            <h3 class="panel-title">Ligipääs puudub</h3>
	        </div>
	        <div class="panel-body">Antud kasutajal ei ole piisavaid õigusi, pöörduge administraatori poole.Kontakt andmete saamiseks <li><a href="kontakt.php">Kliki siia</a></li></h1></div>
	    </div>
	<?php endif; ?>
</div>