<?php
require_once "database.php";
if(sisselogitud() && onAdmin($_SESSION['id'])){
	//Faili üles laadimine//
	$target_dir = "uploads/";
	$target_file = $target_dir . basename($_FILES["pilt"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["pilt"]["tmp_name"]);
		if($check !== false) {
			echo "File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			echo "File is not an image.";
			$uploadOk = 0;
		}
		if (move_uploaded_file($_FILES["pilt"]["tmp_name"], $target_file)) {
			echo "The file ". basename( $_FILES["pilt"]["name"]). " has been uploaded.";
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
	if(!lisaToode($_POST['kategooria'], $_POST['nimi'], $_POST['kirjeldus'], $_POST['hind'], $_POST['kogus'], $target_file)){
		$_SESSION['query_msg'] = 'Toote lisamine ebaõnnestus!';
	}else{
		$_SESSION['query_msg'] = 'Toode lisati andmebaasi!';
	}
	header('Location: lisatoode.php');
}
?>